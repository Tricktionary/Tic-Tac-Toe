public class TicTacToePlayer{

  /** 
   * Player's name. The name cannot be changed once it is set by the construtor.
   */
  private final String name;  // DO NOT CHANGE THIS
  private char c;   
  /** Getter for the players name
    * 
    * @return the name of the player
    */
  public String getName(){   // DO NOT CHANGE THIS
    return name; 
  }
  public char getC(){  
    return c; 
  }
     
  
  
  /**
   * Constructor for a tic tac toe player.
   * 
   * @param name is the name of the player.
   * @param p must be either 'x' or 'o'. 
   */
  public TicTacToePlayer(String name, char p){
    //
    // you define this constructor
    //
    this.name = name;
    this.c = p;
    }
    
     
  /**
   * Checks if a given game instance is over or not.
   * 
   * @param game is an instance of a tic tac toe game (the board)
   * @return true if the game instance is over 
   * (if one player has one or if there is a tie), otherwise false.
   */  
  public static boolean gameOver(TicTacToeGame game){
    //
    // your code here
    //
    //row ||column
    int dime = game.getDimension();
    int size = dime*dime;
    int countx = 0;
    int counto = 0;
    //-------------X----------------------------------
    //horizontal
    for (int x=0;x<size;x++){
      if(game.getAtPosition(x) == 'x'){
        countx++;
      }
      if(game.getAtPosition(x) == 'o'){
        counto++;
      }
      if((countx == dime)||(counto == dime)){
        return true;
      }
      if(((x+1)%(dime)) == 0){
        countx = 0;
        counto = 0;
      }
        
     }
    countx = 0;
    counto = 0;
    
    //vertical
    int vert = dime*(dime-1);
    for(int i = 0; i<dime; i++){
      for(int x = i; x<=vert+i;x+=dime){
        if(game.getAtPosition(x) == 'x'){
          countx++;
        }
        if(game.getAtPosition(x) == 'o'){
          counto++;
        }
        if((countx == dime)||(counto == dime)){
          return true;
        }
      }
      countx = 0;
      counto = 0;
    }
    countx = 0;
    counto = 0;

    //Diagonal backwards down
    int df = dime - 1;
    for(int x = df; x <= df*dime; x+=df){
        if(game.getAtPosition(x) == 'x'){
          countx++;
        }
        if(game.getAtPosition(x) == 'o'){
          counto++;
        }
        if((countx == dime)||(counto == dime)){
          return true;
        }    
    }
    countx = 0;
    counto = 0;

    //Diagonal forwards down
    int db = dime + 1;
    for(int x = 0; x <=db*(dime-1); x+=db){
        if(game.getAtPosition(x) == 'x'){
          countx++;
        }
        if(game.getAtPosition(x) == 'o'){
          counto++;
        }
        if((countx == dime)||(counto == dime)){
          return true;
        }    
    }
    //Checking for ties
    int counterTie =0;
    for (int x=0;x<size;x++){
      if((game.getAtPosition(x) == 'x')||(game.getAtPosition(x) == 'o')){
        counterTie ++;
      
 
      if(counterTie == size){
        return(true);
      }
     }                       
    }
      
      
    return(false);
    }
  
//--------------------------------------------------------------------------------------  
  /**
   * Checks if a given game instance is over or not.
   * 
   * @param game is an instance of a tic tac toe game (the board)
   * @param p1 is one player in the game
   * @param p2 is the other player in the game
   * @return p1 if player p1 has won the game, p2 if player p2 has 
   * won the game and returns null otherwise (if there is no winner in the given game).
   */
  public static TicTacToePlayer winner(TicTacToeGame game, 
                                       TicTacToePlayer p1,
                                       TicTacToePlayer p2)
  {
    //
    // your code here
    //
    char c1 = p1.getC();
    char c2 = p2.getC();
    
    int dime = game.getDimension();
    int size = dime*dime;
    int countx = 0;
    int counto = 0;
    //-------------X----------------------------------
    //horizontal
    for (int x=0;x<size;x++){
 
      if(game.getAtPosition(x) == 'x'){
        countx++;
      }
      if(game.getAtPosition(x) == 'o'){
        counto++;
      }
      if(countx == dime){ 
        if (c1 =='x'){
          return (p1);
        }
        if (c2 =='x'){
          return (p2);
        }        
      }
      if(counto == dime){
        if (c1 =='o'){
          return (p1);
        }
        if (c2 =='o'){
          return (p2);
        } 
      }
      if(((x+1)%(dime)) == 0){
        countx = 0;
        counto = 0;
      }
    }
    countx = 0;
    counto = 0;
    
    //vertical
    int vert = dime*(dime-1);
    for(int i = 0; i<dime; i++){
      for(int x = i; x<=vert+i;x+=dime){
        if(game.getAtPosition(x) == 'x'){
          countx++;
        }
        if(game.getAtPosition(x) == 'o'){
          counto++;
        }
        if(countx == dime){ 
          if (c1 =='x'){
            return (p1);
          }
          if (c2 =='x'){
            return (p2);
          }        
        }
        if(counto == dime){
          if (c1 =='o'){
            return (p1);
          }
          if (c2 =='o'){
            return (p2);
          } 
        }
      }
     countx = 0;
     counto = 0;
    }
    countx = 0;
    counto = 0;

    //Diagonal backwards down
    int df = dime - 1;
    for(int x = df; x <= df*dime; x+=df){
        if(game.getAtPosition(x) == 'x'){
          countx++;
        }
        if(game.getAtPosition(x) == 'o'){
          counto++;
        }
        if(countx == dime){ 
          if (c1 =='x'){
            return (p1);
          }
          if (c2 =='x'){
            return (p2);
          }        
        }
        if(counto == dime){
          if (c1 =='o'){
            return (p1);
          }
          if (c2 =='o'){
            return (p2);
          } 
        }
    }
    countx = 0;
    counto = 0;

    //Diagonal forwards down
    int db = dime + 1;
    for(int x = 0; x <=db*(dime-1); x+=db){
        if(game.getAtPosition(x) == 'x'){
          countx++;
        }
        if(game.getAtPosition(x) == 'o'){
          counto++;
        }
        if(countx == dime){ 
          if (c1 =='x'){
            return (p1);
          }
          if (c2 =='x'){
            return (p2);
          }        
        }
        if(counto == dime){
          if (c1 =='o'){
            return (p1);
          }
          if (c2 =='o'){
            return (p2);
          } 
        }  
    }
    countx = 0;
    counto = 0;
    
    
    return null;
  }
  
  
  
  /** 
   * Checks if current player is playing x's or o's.
   * 
   * @param none
   * @return true if this object is playing x's and false if the object is playing o's.
   */
  public boolean isX(){
    if(getC() == 'x'){
      return true;
    }
    else{
      return false;
    }
  }
  
  /**
   * Getter method that tells if player is playing x's or o's.
   * 
   * @return 'x' if this player is playing x's and returns 'o' if this player is playing o's.
   */
  public char getXO(){
    //
    // your code here
    if(c == 'x'){
      return('x');
    }
    else{
      return('o');
    }
  }
  
  
  /**
   * Finds a valid move in a tic-tac-toe game for this player
   * 
   * @param game is an instance of a tic-tac-toe game
   * @return A position in the board [0,D-1] that is a valid move for this player to make, 
   * where D = dimention*dimension. 
   * If there are multiple valid moves any is acceptable. 
   * If there are no valid moves then the function returns -1.
   */
  public int findMove(TicTacToeGame game){
    int dime = game.getDimension();
    int size = dime*dime;
    for (int x=0;x<size;x++){
      if(game.getAtPosition(x) == '\0'){
        return x;
      }
    }
    return -1;
  }
  
  /**
   * Finds a valid move in a tic-tac-toe game for this player
   * 
   * @param game is an instance of a tic-tac-toe game
   * @return An array of positions in the board [0,D-1] that are each a valid move 
   * for this player to make, where D = dimension*dimension. 
   * All valid moves must be included in the output. 
   * The order of the positions in the output array do not matter. If there are no
   * valid moves then the function return null. 
   */
  public int[] findAllMoves(TicTacToeGame game){
    int dime = game.getDimension();
    int size = dime*dime;
    int counter1 = 0;
    for (int x=0;x<size;x++){
      if(game.getAtPosition(x) == '\0'){
        counter1++;
      }
    }
    int counter2 = 0;
    int[] allMoves = new int[counter1];
    for (int x=0;x<size;x++){
      if(game.getAtPosition(x) == '\0'){
        allMoves[counter2] = game.getAtPosition(x);
        counter2++;
      }
    }
    if (counter1 == 0){
      return null;
    }
    else{
      return(allMoves);
    }
  }
//--------------------------------------------------------------------------------------------------------------------------------
  /**
   * Finds a winning move if possible for this player.
   * 
   * @param game is an instance of a tic-tac-toe game
   * @return A position in the board [0,D-1] that is a valid winning move for this player to make, 
   * where D = dimension*dimension.
   * If there are multiple winning moves then any is acceptable. 
   * Returns -1 if there is no winning move for the player. 
   */
   
  public int findWinningMove(TicTacToeGame game){
    //
    // your code here
    //
    char c = getC();
    int genCounter = 0;
    int dime = game.getDimension();
    int size = dime*dime;
    int countx = 0;
    int spot = -1;
    //-------------X----------------------------------
    //horizontal
    for (int x=0;x<size;x++){
      //System.out.println(countx);
      if(game.getAtPosition(x) == c){
        countx++;    
      }
      if((game.getAtPosition(x) == 'x')||(game.getAtPosition(x) == 'o')){
        genCounter++;
      }
      if(game.getAtPosition(x) == '\0'){
        spot = x;
      }
      if(((x+1)%(dime)) == 0){
        if((countx == (dime-1))&&(genCounter!=dime)){
          //System.out.println("Method 1");
          return spot;
        }
        else
          genCounter = 0;
          countx = 0;
      }
        
     }
    countx = 0;
    genCounter = 0 ;
    
    //vertical
    int vert = dime*(dime-1);
    for(int i = 0; i<dime; i++){
      for(int x = i; x<=vert+i;x+=dime){
        if(game.getAtPosition(x) == c){
          countx++;
        }
        if((game.getAtPosition(x) == 'x')||(game.getAtPosition(x) == 'o')){
          genCounter++;
        }
        if(game.getAtPosition(x) == '\0'){
          spot = x;
        }
      }
      if((countx == (dime-1))&&(genCounter != dime )){
        //System.out.println("Method 2");
        return spot;
      }
      else
        genCounter = 0;
        countx = 0;
    }
    countx = 0;
    genCounter = 0;

    //Diagonal backwards down
    int df = dime - 1;
    for(int x = df; x <= df*dime; x+=df){
        //System.out.println(x);
        if(game.getAtPosition(x) == c){
          countx++;
        }
        if(game.getAtPosition(x) == '\0'){
          spot = x;
        }
       }
    if(countx == (dime-1)){
      //System.out.println("Method 3");
      return (spot);
    }
    countx = 0;

    //Diagonal forwards down
    int db = dime + 1;
    for(int x = 0; x <=db*(dime-1); x+=db){
        if(game.getAtPosition(x) == c){
          countx++;
        }
        if(game.getAtPosition(x) == '\0'){
          spot = x;
        }
    }
    if((countx == (dime-1))){
      return spot;
    }
    countx = 0;
      
      
    return(-1);
    }
 
  
  /**
   * Finds a blocking move if possible for this player
   * 
   * 
   * @param game is an instance of a tic-tac-toe game
   * @return A position in the board [0,D-1] that is a valid bocking 
   * move for this player to make, where D = dimension*dimension.
   * If there are multiple blocking moves then any is acceptable.
   * Returns -1 if there is no blocking move for this player.
   */
  public int findBlockingMove(TicTacToeGame game){
    int genCounter = 0;
    char c = getC();
    if (c=='x'){
      c='o';
    }
    if (c=='o'){
      c='x';
    }
    int dime = game.getDimension();
    int size = dime*dime;
    int countx = 0;
    int spot = -1;
    //-------------X----------------------------------
    //horizontal
    for (int x=0;x<size;x++){
      //System.out.println(countx);
      if(game.getAtPosition(x) == c){
        countx++;    
      }
      if((game.getAtPosition(x) == 'x')||(game.getAtPosition(x) == 'o')){
        genCounter++;
      }
      if(game.getAtPosition(x) == '\0'){
        spot = x;
      }
      if(((x+1)%(dime)) == 0){
        if((countx == (dime-1))&&(genCounter!=dime)){
          return spot;
        }
        else{
          countx = 0;
          genCounter = 0;
        }
      }
        
     }
    countx = 0;
    
    countx = 0;
    
    //vertical
    int vert = dime*(dime-1);
    for(int i = 0; i<dime; i++){
      for(int x = i; x<=vert+i;x+=dime){
        if(game.getAtPosition(x) == c){
          countx++;
        }
        if((game.getAtPosition(x) == 'x')||(game.getAtPosition(x) == 'o')){
        genCounter++;
        }
        if(game.getAtPosition(x) == '\0'){
          spot = x;
        }
      }
      if((countx == (dime-1))&&(genCounter!=dime)){
        return spot;
      }
      else{
        countx = 0;
        genCounter = 0;
      }
    }
  
    countx = 0;

    //Diagonal backwards down
    int df = dime - 1;
    for(int x = df; x <= df*dime; x+=df){
        //System.out.println(x);
        if(game.getAtPosition(x) == c){
          countx++;
        }
        if(game.getAtPosition(x) == '\0'){
          spot = x;
        }
       }
    if(countx == (dime-1)){
      //System.out.println("Method 3");
      return (spot);
    }
    countx = 0; 
    
    //Diagonal forwards down
    int db = dime + 1;
    for(int x = 0; x <(dime*dime); x+=db){
        if(game.getAtPosition(x) == c){
          countx++;
        }
        if(game.getAtPosition(x) == '\0'){
          spot = x;
        }
    }
    if((countx == (dime-1))){
      return spot;
    }
    countx = 0;
      
      
    return(-1);
 
  }
  

  /** 
   * Plays a move for this player in a game
   * 
   * @param game is a tic-tac-toe game that the player is playing.
   * @param pos is the position [-1,D-1] in the game that the player is playing a move,
   * where D=dimension*dimension.
   * @return Nothing. The function has the side effect of playing a move on the board, 
   * using this player's symbol (x or o) at the specified position. If the position 
   * is -1 then the function does nothing.
   */
  public void play(TicTacToeGame game, int pos){
    char c = getC();
    String n = getName();
    TicTacToePlayer gg = new TicTacToePlayer(n,c);
    if(pos != -1){
      if (game.getAtPosition(pos) == '\0'){
        game.play(pos,gg);
      }   
    }
  }
  
}
