import java.util.Scanner;  // used for input

public class TicTacToeApp{
  
  public static void main(String[] args){
    Scanner keyboard = new Scanner(System.in); 
    //Prompting User For Info
    System.out.println("Please Enter your name");
    String name = keyboard.nextLine();
    
    System.out.println("Please Enter your prefered piece");
    char   p    = keyboard.next().charAt(0) ;
    
    System.out.println("Please Enter the Dimmension of the board.");
    int dime = keyboard.nextInt();
    
 
    if(p=='X'){
      p = 'x';
    }
    if(p=='O'){
      p = 'o';
    }
    
    char botP= ' ';
    if(p == 'x'){
      botP = 'o';
    }
    if( p == 'o'){
      botP = 'x';
    }
    
    //Making Board
    TicTacToeGame board = new TicTacToeGame(dime);
    //Making Class for player
    TicTacToePlayer p1  = new TicTacToePlayer(name,p);
    //Making Class for Bot
    TicTacToePlayer bot = new TicTacToePlayer("Bot",botP);
    
    int gamesPlayed = 0;
    int ties = 0;
    int wins = 0;
    int loss = 0;
    
    String input = "1" ;
    int    pos = -1  ;
    System.out.println("Game Start");
    System.out.print(board.show());
    input = keyboard.nextLine();
    
    while(true){
      
      //Player 1
      if( TicTacToePlayer.gameOver(board) ){
        TicTacToePlayer winner =  TicTacToePlayer.winner(board,p1,bot);
        if(winner == null){
          System.out.println("It's a tie");
          ties++;
          gamesPlayed++;
          board = new TicTacToeGame(dime);
          System.out.println("New Game");
        }
        if(winner == p1){
          System.out.println(p1.getName()+" has won");
          wins++;
          gamesPlayed++;
          board = new TicTacToeGame(dime);
          System.out.println("New Game");
        }
        if(winner == bot){
          System.out.println(bot.getName()+" has won");
          loss++;
          gamesPlayed++;
          board = new TicTacToeGame(dime);
          System.out.println("New Game");
        }
      }
      else{
        System.out.println("Please enter a move");
        input = keyboard.nextLine();
        if(input.equals("quit")){
          break;
        }
        System.out.println("The player move "+input);
        pos = Integer.parseInt(input);
        p1.play(board, pos);
      }
      
      //Bot-----------------------------------------------------------------------------------------------------------------
      if( TicTacToePlayer.gameOver(board) ){
        TicTacToePlayer winner =  TicTacToePlayer.winner(board,p1,bot);
        if(winner == null){
          System.out.println("It's a tie");
          ties++;
          gamesPlayed++;
          board = new TicTacToeGame(dime);
          System.out.println("New Game");
        }
        if(winner == p1){
          System.out.println(p1.getName()+" has won");
          wins++;
          gamesPlayed++;
          board = new TicTacToeGame(dime);
          System.out.println("New Game");
        }
        if(winner == bot){
          System.out.println(bot.getName()+" has won");
          loss++;
          gamesPlayed++;
          board = new TicTacToeGame(dime);
          System.out.println("New Game");
        }
      }
      else{
        pos =(bot.findWinningMove(board));
        if(pos==-1){
          pos =(bot.findBlockingMove(board));
            if(pos == - 1){
              pos = bot.findMove(board);
              bot.play(board, pos );
              System.out.println("The bot move is "+pos);
            }
            else{
              bot.play(board, pos );
              System.out.println("The bot move is "+pos);
            }
          }
          else{
            bot.play(board, pos);
            System.out.println("The bot move is "+pos);
          }
      }
      System.out.println(board.show());
       
    }
    
    System.out.println("Games Played: "+gamesPlayed);
    System.out.println("Games Won   : "+wins);
    System.out.println("Games Tied  : "+ties);
    System.out.println("Games Loss  : "+loss);
  }
}
